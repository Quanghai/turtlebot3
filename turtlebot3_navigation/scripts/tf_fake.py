#!/usr/bin/env python  
import roslib

import rospy
import tf
from nav_msgs.msg import Odometry
import numpy as np
from tf.transformations import euler_from_quaternion, quaternion_from_euler

    
if __name__ == "__main__":
    rospy.init_node('turtlebot3_tf_fake', anonymous=True) #make node 

    rate = rospy.Rate(10.0)
    br = tf.TransformBroadcaster()
    
    while not rospy.is_shutdown(): 
        # map_quaternion = quaternion_from_euler(0,0,0)
        # br.sendTransform((0.0, 0.0, 0.0), 
        #                 (map_quaternion[0], map_quaternion[1], map_quaternion[2], map_quaternion[3]),
        #                 rospy.Time.now(),
        #                 "odom",
        #                 "map")   

        map_quaternion = quaternion_from_euler(-1.570796,0,0)
        br.sendTransform((0.000, 0.080, 0.023), 
                        (map_quaternion[0], map_quaternion[1], map_quaternion[2], map_quaternion[3]),
                        rospy.Time.now(),
                        "wheel_left_link",
                        "base_link")  

        map_quaternion = quaternion_from_euler(-1.570796,0,0)
        br.sendTransform((0.000, -0.080, 0.023),
                        (map_quaternion[0], map_quaternion[1], map_quaternion[2], map_quaternion[3]),
                        rospy.Time.now(),
                        "wheel_right_link",
                        "base_link")  
    
        rate.sleep()
    rospy.spin()